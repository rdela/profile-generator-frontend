# Profile Picture Generator

## Usage

### Build development

```bash
npm run build:dev
```

- serve `./dist` folder, which contains a `index.html`
- all files got source-maps for better debugging

You can run:

```bash
npm run assets:watch
```

 when developing.

### Build production

```bash
npm run build:dev
```

and use `./dist/main.js` file like in `./dist/index.html`:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Profile Generator</title>
  <link rel="shortcut icon" href="logo.png">
  <script type="text/javascript" src="main.js"></script></head>
  <body>
<div class="container"></div>
    <script type="application/javascript">
        (function () {
          window.ProfilePictureGenerator({
            container: document.querySelector('.container')
          });
        })();
    </script>
  </body>
</html>
```

The possible settings are:

- `container`: pass a container node to render the generator into.
- `tracking`: if set to false no tracking script will be included
- `uploadButtonTitle`: text for the upload button
- `languageSelectorLabel`: text for the language selector (aria label)
- `imageAltText`: text alternative for the image
- `imagePreview`: url of the preview image
- `templateUrl`: url of the template image
- `downloadButtonTitle`: text of the download button
- `fileName`: name of the generated image file
- `initialLanguage`: inital language of the generator, if it is not set the language provided by the browser will be used
- `languages`: all these settings (except for `container`, `tracking` and `initialLanguage`) for the specific languages (see example)

```javascript
{
  container: document.querySelector('.container'),
  tracking: true,
  uploadButtonTitle: 'Eigenes Profilbild generieren',
  languageSelectorLabel: 'Language Selector',
  imageAltText: 'Generiertes Fridays for Future Profilbild',
  templateUrl: '/assets/template.png',
  imagePreview: '/assets/preview.png',
  downloadButtonTitle: 'Profilbild herunterladen',
  fileName: 'ProfilePicture.png',
  languages: {
    de: {
        uploadButtonTitle: 'Eigenes Profilbild generieren',
        downloadButtonTitle: 'Profilbild herunterladen',
        fileName: 'ProfilePicture.png',
        templateUrl: '/assets/template-de.png',
        imagePreview: '/assets/preview-de.png'
    },
    "en": {
        uploadButtonTitle: 'Generate your own profile picture',
        downloadButtonTitle: 'Download',
        imageAltText: 'Generated Fridays for Future profile image'
        fileName: 'ProfilePicture.png',
        templateUrl: '/assets/template-en.png',
        imagePreview: '/assets/preview-en.png'
    }
  }
}
```

If the settings for languages, templateUrl and imagePreview haven't been changed the content of the `dist/src` folder also needs to be available.

To run the docker-compose version here, change in `src/index.ejn`file to:

```javascript
{
    container: document.querySelector('.container'),
    imagePreview: `http://0.0.0.0:8094/src/templates/de/preview.png`,
    templateUrl: `http://0.0.0.0:8094/src/templates/de/template.png`,
    fileName: 'ProfilePicture.png',
    languages: {
        de: {
            uploadButtonTitle: 'Eigenes Profilbild generieren',
            downloadButtonTitle: 'Profilbild herunterladen',
            fileName: 'ProfilePicture.png',
            templateUrl: `http://0.0.0.0:8094/src/templates/de/template.png`,
            imagePreview: `http://0.0.0.0:8094/src/templates/de/preview.png`
        },
        en: {
            uploadButtonTitle: 'Generate your own profile picture',
            downloadButtonTitle: 'Download',
            fileName: 'ProfilePicture.png',
            templateUrl: `http://0.0.0.0:8094/src/templates/en/template.png`,
            imagePreview: `http://0.0.0.0:8094/src/templates/en/preview.png`
        },
        es: {
            uploadButtonTitle: 'Generate your own profile picture',
            downloadButtonTitle: 'Download',
            fileName: 'ProfilePicture.png',
            templateUrl: `http://0.0.0.0:8094/src/templates/es/template.png`,
            imagePreview: `http://0.0.0.0:8094/src/templates/es/preview.png`
        },
        fr: {
            uploadButtonTitle: 'Generate your own profile picture',
            downloadButtonTitle: 'Download',
            fileName: 'ProfilePicture.png',
            templateUrl: `http://0.0.0.0:8094/src/templates/fr/template.png`,
            imagePreview: `http://0.0.0.0:8094/src/templates/fr/preview.png`
        }
    }
  });
}
```

## Tracking

By default the profile picture generator will track it's views, when a image is generated and downloaded to an analytics server from DevelopersForFuture. The uploaded images or information about these is never send to any server. This can be disabled in the settings by setting `tracking` to false. The tracking also respects the doNotTrack information send by the browser.

## License

[License](LICENSE.md)
