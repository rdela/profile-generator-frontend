FROM node:10-alpine as builder

# install and cache app dependencies
COPY / /

# add `/app/src/node_modules/.bin` to $PATH
ENV PATH /app/src/node_modules/.bin:$PATH
WORKDIR /app/src


# install and cache app dependencies
RUN npm install &&  npm run build:prod

FROM nginx:1.15.1-alpine

COPY ./etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/src/dist /usr/share/nginx/html


ENV VERSION_TAG m4ProjectVersion()
LABEL image.name=m4ProjectName() \
      image.version=m4ProjectVersion() \
      image.tag=m4ReleaseImage() \
      image.scm.commit=$commit \
      image.scm.url=m4GitOriginUrl() \
      image.author="Maximilian Berghoff <maximilian.berghoff@gmx.de>"
