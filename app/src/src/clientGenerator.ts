import { getOrientation, orient, EXIFOrientation, fileToDataURL } from "./util";

import avatarLayers from "./avatar";
export type ColorLayer = {
  color: string;
};

export type RectangleLayer = {
  type: "RECTANGLE";
  offset: number;
  size: number;
} & ColorLayer;

export type CircleLayer = {
  type: "CIRCLE";
  center: number;
  width: number;
  radius: number;
} & ColorLayer;

export type ImageLayer = {
  type: "IMAGE";
  offset: number;
  size: number;
  src: string;
  rotation: EXIFOrientation;
};

export type TextCircleLayer = {
  type: "TEXT_CIRCLE";
  text: string;
  fontSize: number;
  font: string;
  angle: number;
  center: number;
  radius: number;
  facingInward: boolean;
} & ColorLayer;

export type CanvasLayer =
  | RectangleLayer
  | CircleLayer
  | ImageLayer
  | TextCircleLayer;

type Ctx = CanvasRenderingContext2D | OffscreenCanvasRenderingContext2D;

async function generateCanvas(layers: CanvasLayer[], size: number) {
  const canvas = window["OffscreenCanvas"]
    ? new OffscreenCanvas(size, size)
    : document.createElement("canvas");
  canvas.width = size;
  canvas.height = size;

  const ctx = canvas.getContext("2d");

  for (const layer of layers) {
    await drawLayer(ctx, layer, size);
  }

  return canvas;
}

function drawLayer(ctx: Ctx, layer: CanvasLayer, size: number) {
  switch (layer.type) {
    case "RECTANGLE":
      return drawRectangle(ctx, layer);
    case "CIRCLE":
      return drawCircle(ctx, layer);
    case "IMAGE":
      return drawImage(ctx, layer, size);
    case "TEXT_CIRCLE":
      return drawTextCircle(ctx, layer);
  }
}

function drawTextCircle(
  ctx: Ctx,
  {
    text,
    angle,
    center,
    radius,
    font,
    fontSize,
    facingInward,
    color
  }: TextCircleLayer
) {
  ctx.save();
  ctx.fillStyle = color;
  ctx.font = `${fontSize}px ${font}`;
  ctx.textBaseline = "middle";
  ctx.textAlign = "center";

  if (facingInward) {
    radius = radius * -1;
  } else {
    angle += Math.PI;
  }

  const startAngle: number = calculateStartAngle(ctx, angle, text, radius);

  ctx.translate(center, center);
  ctx.rotate(startAngle);

  for (let i = 0; i < text.length; i++) {
    const char = text[i];
    const charWidth: number = ctx.measureText(char).width;
    const charAngle: number = charWidth / radius;
    ctx.rotate(-charAngle / 2);
    ctx.fillText(char, 0, radius);
    ctx.rotate(-charAngle / 2);
  }

  ctx.restore();
}

function calculateStartAngle(
  ctx: Ctx,
  startAngle: number,
  text: string,
  radius: number
): number {
  const textWidth = ctx.measureText(text).width;
  const textAngle = textWidth / radius;
  return startAngle + textAngle / 2;
}

async function drawImage(
  ctx: Ctx,
  { src, size, offset, rotation }: ImageLayer,
  canvasSize: number
): Promise<void> {
  const img = new Image();
  img.crossOrigin = "anonymous";
  img.src = src;
  // Wait for the image to be loaded so it can be copied to the canvas.
  // If it isn't loaded an empty image would be copied.
  await new Promise(resolve => img.addEventListener("load", resolve));

  const aspectRatio = img.width / img.height;

  ctx.save();

  orient(ctx, rotation, canvasSize);

  if (img.width > img.height) {
    ctx.drawImage(
      img,
      (img.width - img.height) / 2,
      0,
      img.height,
      img.height,
      offset,
      offset,
      size,
      size
    );
  } else {
    ctx.drawImage(
      img,
      0,
      (img.height - img.width) / 2,
      img.width,
      img.width,
      offset,
      offset,
      size,
      size
    );
  }

  ctx.restore();
}

function drawCircle(ctx: Ctx, { center, radius, width, color }: CircleLayer) {
  ctx.save();
  ctx.strokeStyle = color;
  ctx.lineWidth = width;
  ctx.beginPath();
  ctx.arc(center, center, radius, 0, Math.PI * 2, true);
  ctx.stroke();
  ctx.restore();
}

function drawRectangle(ctx: Ctx, { offset, size, color }: RectangleLayer) {
  ctx.save();
  ctx.fillStyle = color;
  ctx.fillRect(offset, offset, size, size);
  ctx.restore();
}

export async function clientBuildImageUrl(
  file: File,
  size: number,
  templateUrl: string
) {
  const uploadDataUrl = await fileToDataURL(file);

  const layers: CanvasLayer[] = avatarLayers({
    size: size,
    template: templateUrl,
    src: uploadDataUrl,
    srcRotation: (await getOrientation(file)) || 1,
    srcOffsetX: 0,
    srcOffsetY: 0,
    zoomFactor: 1
  });

  const canvas = await generateCanvas(layers, size);
  let url = "";
  if (canvas instanceof HTMLCanvasElement) {
    url = canvas.toDataURL("image/png");
  } else {
    const blob = await canvas.convertToBlob();
    url = URL.createObjectURL(blob);
  }

  return url;
}
