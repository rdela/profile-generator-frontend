import { EXIFOrientation } from "./util";
import { CanvasLayer } from "./clientGenerator";

export type AvatarOptions = {
  size: number;
  template: string;
  src: string;
  srcRotation: EXIFOrientation;
  srcOffsetX: number;
  srcOffsetY: number;
  zoomFactor: number;
};

export default function({
  template,
  size,
  src,
  srcRotation
}: AvatarOptions): CanvasLayer[] {
  return [
    {
      type: "RECTANGLE",
      offset: 176 * (size / 1200),
      size: size - 176 * 2 * (size / 1200),
      color: "#1b7340"
    },
    {
      type: "IMAGE",
      offset: 176 * (size / 1200),
      size: size - 176 * 2 * (size / 1200),
      src,
      rotation: srcRotation
    },
    {
      type: "IMAGE",
      offset: 0,
      size: size,
      src: template,
      rotation: 1
    }
  ];
}
