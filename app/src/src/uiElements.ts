import { dataUrlToBlob } from "./util";

export function createFileUpload(
  container: HTMLElement,
  text: string,
  className: string
) {
  const fileInput = document.createElement("input");
  fileInput.type = "file";
  fileInput.accept = "image/*";
  fileInput.style.display = "none";
  const fileInputLabel = document.createElement("label");
  fileInputLabel.classList.add(className);
  fileInputLabel.tabIndex = 0;
  const fileInputLabelText = document.createElement("span");
  fileInputLabelText.textContent = text;
  container.appendChild(fileInputLabel);
  fileInputLabel.appendChild(fileInputLabelText);
  fileInputLabel.appendChild(fileInput);
  return { fileInput, fileInputLabel: fileInputLabelText };
}

export function createLanguageSelector(
  container: HTMLElement,
  languages: {id: string, name: string}[],
  className: string,
  ariaLabel: string
) {
  const select = document.createElement("select");
  select.classList.add(className);
  select.setAttribute("aria-label", ariaLabel);

  for (const {id, name} of languages.sort(({name: a}, {name: b}) => a.localeCompare(b))) {
    const option = document.createElement("option");
    option.value = id;
    option.textContent = name;
    select.appendChild(option);
  }
  container.appendChild(select);

  return select;
}

export function createImageElement(
  container: HTMLElement,
  previewImage: string,
  altText: string,
  className: string
): HTMLImageElement {
  const imageContainer = document.createElement("div");
  imageContainer.classList.add(className);
  container.appendChild(imageContainer);
  const img = new Image();
  img.style.width = "100%";
  img.style.height = "100%";
  img.src = previewImage;
  img.alt = altText;
  imageContainer.appendChild(img);
  return img;
}

export function createDownloadElement(
  container: HTMLElement,
  text: string,
  fileName: string,
  className: string
) {
  const downloadLink = document.createElement("a");
  downloadLink.classList.add(className);
  downloadLink.textContent = text;
  downloadLink.download = fileName;
  downloadLink.style.display = "none";
  container.appendChild(downloadLink);

  // Handle download for IE and EDGE
  downloadLink.addEventListener("click", () => {
    if (navigator.msSaveBlob) {
      navigator.msSaveBlob(dataUrlToBlob(downloadLink.href), fileName);
    }
  });

  return downloadLink;
}

export function createLoadingScreen(container: HTMLElement) {
  const loadingScreen = document.createElement("div");
  loadingScreen.classList.add("loading-screen");
  loadingScreen.innerHTML = `
        <div class="fff-profile-picture-generator-spinner">
            <div class="fff-profile-picture-generator-spinner1 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner2 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner3 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner4 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner5 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner6 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner7 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner8 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner9 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner10 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner11 fff-profile-picture-generator-spinner-child"></div>
            <div class="fff-profile-picture-generator-spinner12 fff-profile-picture-generator-spinner-child"></div>
        </div>`;
  loadingScreen.style.display = "none";
  container.appendChild(loadingScreen);

  return loadingScreen;
}

export function createPrivacyNote(container: HTMLElement, text: string) {
  const div = document.createElement("div");
  div.innerHTML = text;
  div.className = "fff-profile-picture-generator--privacy-note";

  container.appendChild(div);

  return div;
}

export function createPrivacyLink(
  container: HTMLElement,
  text: string,
  link: string
) {
  const a = document.createElement("a");
  a.innerText = text;
  a.className = "fff-profile-picture-generator--privacy-note";
  a.href = link;

  container.appendChild(a);

  return a;
}
