const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest')

module.exports = {
  mode: 'production',
  entry: ['babel-polyfill', './src/main.ts'],
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          }
        }
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.ttf$/i,
        use: ['url-loader'],
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
        },
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Profile Generator',
      favicon: 'src/logo.png',
      template: 'src/index.ejs',
      inject: 'head'
    }),
    new WebpackPwaManifest({
      short_name: "FFF PB",
      name: "Fridays for Future Profile Picture Generator",
      description: 'Generate your own Fridays for Future profile picture!',
      background_color: '#ffffff',
      background_color: "#1B7340",
      display: "standalone",
      theme_color: "#1DA64A",
      icons: [
        {
          src: path.resolve('src/templates/de/preview.png'),
          sizes: [96, 128, 192, 256, 384, 512, 1024]
        }
      ]
    }),
    new WorkboxPlugin.GenerateSW({
       clientsClaim: true,
      skipWaiting: true,
    }),
    new CopyPlugin([
      'src/Jost-600-Semi.ttf',
      'src/templates/**/*.png'
    ])
  ]
};