#!/bin/bash

set -e

HOST=${DEPLOY_HOST-example.org}
USER=${DEPLOY_USER-user}
SERVER_PATH=${DEPLOY_JS_PATH-/www/html/public/js}
SERVER_PATH_TEMPLATES=${DEPLOY_TEMPLATE_PATH-/www/html/public/js}
PUBLIC_JS_PATH=${PUBLIC_JS_PATH-/js}
PUBLIC_TEMPLATE_PATH=${PUBLIC_TEMPLATE_PATH-/images/templates}
TMP_FOLDER=/tmp/pb
VERSION=$1
REPOSITORY=https://gitlab.com/developersforfuture/profile-generator-frontend.git

fail() {
    cd -
    rm -rf $TMP_FOLDER/$VERSION
    echo "$*" >&2
    exit 1
}

clone () {
    giturl="$1"
    target="$2"
    version="$3"
    ref="master"
    # see https://tribut.de/blog/git-commit-signatures-trusted-keys
    gitgpgopts="-c gpg.program=selfgpg"
    tags=$(git ls-remote --tags $giturl | grep $version)
    [ -e "$tags" ] && fail "No version $version found at $giturl"
    [ -e "$target" ] && fail "$target already exists"
    git clone $giturl $target
    #git ls-remote --exit-code "$giturl" "$ref" >/dev/null ||
    #fail "$giturl does not exists or has no $ref branch"
    #git init -- "$target"
    cd -- "$target"
    #git remote add origin -- "$giturl"
    #git fetch -q --progress origin
    # add it when having signed commits again
    #git $gitgpgopts verify-commit "origin/$ref" ||
    #fail "origin/$ref does not have a valid signature"
    #git $gitgpgopts merge --ff-only --verify-signatures "origin/$ref"
    
    #git branch -q --set-upstream-to="origin/$ref"
    git checkout $version9
}

deploy () {
    scp app/src/dist/main.js $USER@$HOST:$SERVER_PATH/main_v${VERSION}.js

    for language in de es en fr ;
    do
        scp -r app/src/dist/src/templates/${language}/preview.png $USER@$HOST:${SERVER_PATH_TEMPLATES}/${language}_preview_${VERSION}.png
        scp -r app/src/dist/src/templates/${language}/template.png $USER@$HOST:${SERVER_PATH_TEMPLATES}/${language}_template_${VERSION}.png
    done

}

html_hint () {
    echo " + + + Use the following HTML snipped for you page + + + "
    echo |
        "<div class=\"container\"></div>
        <script type=\"application/javascript\">
            (function () {
            window.ProfilePictureGenerator({
                container: document.querySelector('.container'),
                imagePreview: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/de_preview.png',
                templateUrl: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/de_template.png',
                fileName: 'ProfilePicture.png',
                languages: {
                    de: {
                        uploadButtonTitle: 'Eigenes Profilbild generieren',
                        downloadButtonTitle: 'Profilbild herunterladen',
                        fileName: 'ProfilePicture.png',
                        templateUrl: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/de_${VERSION}_template.png',
                        imagePreview: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/de_${VERSION}_preview.png'
                    },
                    en: {
                        uploadButtonTitle: 'Generate your own profile picture',
                        downloadButtonTitle: 'Download',
                        fileName: 'ProfilePicture.png',
                        templateUrl: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/en_${VERSION}_template.png',
                        imagePreview: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/en_${VERSION}_preview.png'
                    },
                    es: {
                        uploadButtonTitle: 'Generate your own profile picture',
                        downloadButtonTitle: 'Download',
                        fileName: 'ProfilePicture.png',
                        templateUrl: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/es_${VERSION}_template.png',
                        imagePreview: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/es_${VERSION}_preview.png'
                    },
                    fr: {
                        uploadButtonTitle: 'Generate your own profile picture',
                        downloadButtonTitle: 'Download',
                        fileName: 'ProfilePicture.png',
                        templateUrl: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/fr_${VERSION}_template.png',
                        imagePreview: 'https://${HOST}${PUBLIC_TEMPLATE_PATH}/fr_${VERSION}_preview.png'
                    }
                }
            });
            })();
        </script>"
}


[ "$#" -ne 1 ] && fail "call me like this: $(basename "$0") version"
clone $REPOSITORY $TMP_FOLDER/$VERSION $VERSION
deploy
html_hint

